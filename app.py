import json
import os.path

from functools import wraps
from typing import Dict, Union, Optional, TYPE_CHECKING, Tuple, Callable, Any
from urllib.parse import urlparse

import click
import requests
from flask import Flask, request, Response, jsonify, redirect
from flask.typing import ResponseValue
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from hashids import Hashids
import yaml

app = Flask(__name__)

app.config.from_file("config.yaml", load=yaml.safe_load)

db = SQLAlchemy()
db.init_app(app)
migrate = Migrate()
migrate.init_app(app, db)

hashids = Hashids(min_length=5, salt=app.config['SECRET_KEY'])

Pool = Dict[str, Union[Dict[str, str], str]]

data: Optional[Dict[str, Pool]] = None

BASE_URL = os.environ.get("REDIRECTOR_BASE_URL") or "http://127.0.0.1:5000/"
API_KEY = os.environ.get("REDIRECTOR_API_KEY") or None


@app.cli.command('short')
@click.argument('url')
def get_short_link(url: str) -> None:
    url = BASE_URL + f"link?url={url}&type=short"
    if API_KEY:
        url += f"&key={API_KEY}"
    resp = requests.get(url)
    try:
        print(json.loads(resp.text)["url"])
    except requests.exceptions.JSONDecodeError:
        print(resp.text)


@app.cli.command('direct')
@click.argument('url')
def get_direct_link(url: str) -> None:
    url = BASE_URL + f"link?url={url}&type=direct"
    if API_KEY:
        url += f"&key={API_KEY}"
    resp = requests.get(url)
    try:
        print(json.loads(resp.text)["url"])
    except requests.exceptions.JSONDecodeError:
        print(resp.text)


@app.cli.command('resolve')
@click.argument('hash')
def resolve_link(hash: str) -> None:
    url = BASE_URL + hash
    resp = requests.get(url, allow_redirects=False)
    print(resp.headers["Location"])


def load_initial_data() -> None:
    global data
    data = json.load(open("data.json"))
    print("Loaded initial data from filesystem")
    if TYPE_CHECKING:
        assert data is not None  # nosec B101
    for pool in data["pools"]:
        if TYPE_CHECKING:
            assert isinstance(pool, dict)  # nosec B101
        print(f"{pool['short_name']}: {pool['api_key']}")


if os.path.exists("data.json"):
    load_initial_data()


class Link(db.Model):  # type: ignore[name-defined,misc]
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    pool = db.Column(db.String())
    origin_domain_name = db.Column(db.String())
    url_path = db.Column(db.String())


def parse_url(url: str) -> Tuple[str, str]:
    parsed_url = urlparse(url)
    domain_name = parsed_url.netloc.split(":")[0].lower()
    return domain_name, parsed_url.path


def lookup_pool(key: Optional[str] = None,
                name: Optional[str] = None) -> Optional[Pool]:
    if data is None:
        return None  # We don't know about the pools yet
    if key is None and name is None:
        return None  # Nothing is going to match nothing
    for pool in data['pools']:
        if TYPE_CHECKING:
            assert isinstance(pool, dict)  # nosec B101
        if key is not None and pool['api_key'] == key:
            return pool
        if name is not None and pool['short_name'] == name:
            return pool
    return None


def lookup_domain(pool: Pool, domain_name: str) -> Optional[str]:
    origins = pool["origins"]
    if TYPE_CHECKING:
        assert isinstance(origins, dict)  # nosec B101
    return origins.get(domain_name, None)


def generate_link(pool: Pool, url: str, type_: str) -> ResponseValue:
    if type_ not in ["direct", "short"]:
        raise NotImplementedError("generate_link")
    domain_name, path = parse_url(url)
    if not lookup_domain(pool, domain_name):
        return not_found()
    if type_ == "direct":
        return jsonify({
            "url": generate_link_direct(pool, domain_name, path)
        })
    # It's a short link, because we narrowed down what it could be in the very
    # first if statement of this function.
    return jsonify({
        "url": generate_link_short(pool, domain_name, path)
    })


def not_found() -> ResponseValue:
    return Response("No link could be generated for the given parameters",
                    status=404)


def generate_link_direct(pool: Pool, domain_name: str, path: str) -> str:
    mirror = lookup_domain(pool, domain_name)
    query = (f"utm_medium={app.config['UTM_MEDIUM']}&"
             f"utm_campaign={app.config['UTM_CAMPAIGN']}&"
             f"utm_source={app.config['UTM_SOURCE']}")
    return f"{mirror}{path}?{query}"


def generate_link_short(pool: Pool, domain_name: str, path: str) -> str:
    pool_name = pool["short_name"]
    link: Optional[Link] = Link.query.filter(Link.pool == pool_name,
                                             Link.origin_domain_name == domain_name,
                                             Link.url_path == path).first()
    if link is None:
        link = Link(pool=pool_name, origin_domain_name=domain_name, url_path=path)
        db.session.add(link)
        db.session.commit()
    return (f"https://{pool['redirector_domain'] or app.config['DEFAULT_REDIRECTOR_DOMAIN']}"
            f"/{hashids.encode(link.id)}")


def authenticated_view(view_function: Callable[..., ResponseValue]) -> Callable[..., ResponseValue]:
    @wraps(view_function)
    def authenticated_function(*args: Any, **kwargs: Any) -> ResponseValue:
        pool = lookup_pool(request.args.get("key", app.config["PUBLIC_KEY"]))
        if not pool:
            return Response("The given API key was invalid.", status=403)
        return view_function(pool, pool["api_key"] == app.config["PUBLIC_KEY"], *args, **kwargs)

    return authenticated_function


@app.route('/me')
@authenticated_view
def route_me(pool: Pool, anonymous: bool) -> ResponseValue:
    return jsonify({
        "anonymous": anonymous,
        "description": pool["description"]
    })


@app.route('/link')
@authenticated_view
def route_link(pool: Pool, anonymous: bool) -> ResponseValue:
    url = request.args['url']
    if anonymous:
        type_ = "direct"
    else:
        type_ = request.args.get('type', 'direct')
    return generate_link(pool, url, type_)


# This uses a different authentication method as it's authorising for update, not link generation.
@app.route('/updt', methods=['POST'])
def update() -> ResponseValue:
    global data
    key = request.headers.get("Authorization")
    if key != f"Bearer {app.config['UPDATE_KEY']}":
        return Response("The given API key was invalid.", status=403)
    data = request.json
    return jsonify({"success": True})


@app.route("/")
def homepage() -> ResponseValue:
    return Response("", 200)

# This has to go last due to the matching rules
@app.route('/<hash_>')
def short_link(hash_: str) -> ResponseValue:
    decoded_hash = hashids.decode(hash_)
    if not decoded_hash:
        return Response("", status=200)
    link = Link.query.filter(Link.id == decoded_hash[0]).first()
    if link is None:
        return Response("", status=200)
    pool = lookup_pool(name=link.pool)
    if pool is None:
        return not_found()
    target = generate_link_direct(
        pool, link.origin_domain_name, link.url_path)
    return redirect(target)


if __name__ == '__main__':
    app.run()
